;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Nikolai Krug"
      user-mail-address "nikoladze@posteo.de")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-tomorrow-day)

(setq doom-font (font-spec :size 15))

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; approximate "hybrid-mode" and fix the most annoying non-matching key bindings
(use-package evil
  :custom
  evil-disable-insert-state-bindings t
  )

;; set/reset some mode specific key bindings
(map! :i "C-SPC" #'set-mark-command)
(map! :i "C-x C-s" #'save-buffer)

;; here i grepped for "M-b" to find that this is set for package `evil-markdown'
;; so i need to rebind it after that
(map! :after evil-markdown
      :map evil-markdown-mode-map
      :i "M-b" #'backward-word)

(setq flycheck-highlighting-mode "columns")
;; in analogy to https://discourse.doomemacs.org/t/disable-smartparens-or-parenthesis-completion/134
(remove-hook 'doom-first-buffer-hook #'global-flycheck-mode)

;; from https://tecosaur.github.io/emacs-config/config.html - section 2.2.1
;; (let's see if this works)
(setq undo-limit 80000000                         ; Raise undo-limit to 80Mb
      evil-want-fine-undo t)                      ; By default while in insert all changes are one big blob. Be more granular

;; don't auto delete trailing space in multiline strings
;; see https://github.com/lewang/ws-butler
(setq ws-butler-trim-predicate
      (lambda (beg end)
        (not (eq 'font-lock-string-face
                 (get-text-property end 'face)))))

;; don't indent org files according to headline level
(add-hook! 'org-mode-hook (org-indent-mode -1))
