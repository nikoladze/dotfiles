#!/usr/bin/env python3
"""
Render the template (.jinja) config files
"""

from jinja2 import Template, Environment, FileSystemLoader
import socket

templates = [
    "i3_config.jinja",
    ".Xresources.jinja",
    ".zshrc.jinja"
]

hostname = socket.gethostname()
var_dict = {}
var_dict['laptop'] = (hostname == "havarius") or (hostname == "gar-nb-etp28")
var_dict['gar_nb_etp28'] = (hostname == "gar-nb-etp28")
var_dict['gar'] = hostname.startswith("gar-ws") or hostname.startswith("gar-cn")
var_dict['cluster'] = ("agkuhr" in hostname)

env = Environment(
    loader=FileSystemLoader("./"),
    trim_blocks=True,
    lstrip_blocks=True
)

for template_path in templates:
    print("Rendering "+template_path)
    template = env.get_template(template_path)
    with open(template_path.replace(".jinja", ""), "w") as f:
        f.write(template.render(**var_dict))
